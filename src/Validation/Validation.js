import React from 'react';

const validation = (props) => {

    let result = '';
    
    if( props.textLength === 0 ) {
        result = 'Please input text, can not be empty';
    } else if ( props.textLength < 5 ) {
        result = 'Text too short';
    } else {
        result = 'Text long enough';
    }

    return(
        <div className="Validation">
            <p>Warning Message: { result }</p>
        </div>
    )   
};

export default validation;