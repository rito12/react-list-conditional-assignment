import React, { Component } from 'react';
import './App.css';
import Validation from './Validation/Validation';
import Character from './Character/Character';

class App extends Component {
	state = {
		userText: '',
	};

	getText = (e) => {
		this.setState({ userText: e.target.value });
	}

	deleteChar = (index) => {
		const text = this.state.userText.split('');
		text.splice(index, 1);

		this.setState({
			userText: text.join('')
		});
	}

	render() {
		const style = {
			display: 'flex',
			justifyContent: 'flex-start',
		};

		let charList = null;

		charList = (
			<div style={style}>
				{this.state.userText.split('').map((text, index) => {
					return <Character
						text={ text }
						key={ index}
						click={(e) => this.deleteChar(index)}
					></Character>
				})}
			</div>
		)

		return (
			<div className="App">

				<input type="text" onChange={this.getText} value={ this.state.userText }></input>
				<p>Length of character: { this.state.userText.length }</p>

				<Validation
					textLength={this.state.userText.length}
				></Validation>

				{ charList }
			</div>
		);
	}
}

export default App;
