import React from 'react';

const character = (props) => {
    const style = {
        border: '1px solid grey',
        padding: '0.5rem',
        margin: '0 .5rem',
        cursor: 'pointer'
    };

    return(
        <div className="Character">
            <p style={style} onClick={props.click}>{props.text}</p>
        </div>
    )   
};

export default character;